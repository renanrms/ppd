# Programação Paralela e Distribuída 2020.1: Trabalhos em Grupo

## Comandos úteis

Para fazer ssh para o servidor:

`ssh -p 22022 $USER@$SERVER`

Para mover o repositório para o servidor, sem os arquivos ocultos do git, IDE e outros:

`scp -rP 22022 ppd/[^.]* $USER@$SERVER:$GROUP_DIR/ppd-$USER/`

Para mover apenas os arquivos de código fonte (pastas src e submission):

`scp -rP 22022 ppd/s* $USER@$SERVER:$GROUP_DIR/ppd-$USER/`

Depois deve-se entrar na pasta, dar make e chamar os arquivos de submissão com qsub **apartir da pasta submission**:

`cd ppd && make`

`cd bin`

`qsub sub{n}.py`

Para copiar os arquivos de saída resultantes do programa para o computador local, pode ser usado o comando (na máquina local):

`scp -rP 22022 $USER@$SERVER:$GROUP_DIR/ppd-$USER/submission/sub*.py.o* ppd/eval/`

Assim os arquivos serão copiados para a pasta eval e podem ter o nome trocado para ficarem no mesmo padrão e podem ser veitas análises dos resultados com os notebooks, por exemplo. É bom fazer uma verificação se não houve nenhuma saída indesejada do arquivo. Um erro que ocorre é o cabeçalho do arquivo de saída ficar no final e não no início, o que deve ser corrigido.
