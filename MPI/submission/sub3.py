#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

#filename = 'minimos-quadrados-original'
#filename = 'minimos-quadrados-bcast'
#filename = 'minimos-quadrados-bsend-reduce'
filename = 'minimos-quadrados-nonblock'

file_header = 'n,n_processes,wtime'
local_csv_path = '../eval/{}.csv'.format(filename)

if 'PBS_O_WORKDIR' in os.environ.keys(): # No servidor com PBS:

	# Vai para o diretório de onde se chamou o qsub depois entra na pasta bin.
	os.chdir(os.environ['PBS_O_WORKDIR'])
	os.chdir('../bin')

	# Printa o cabeçalho.
	print(file_header)

	# Define os parâmetros para rodar como descrito no trabalho.
	Ns = [10**5, 10**6, 10**7]
	n_processes = [1, 2, 4, 8, 12, 16]
	n_repetitions = 10

else: # Rodando localmente:

	# Muda para o diretório do programa (para caso seja chamado de outro local).
	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	os.chdir('../bin')
	
	# Escreve o cabeçalho no arquivo de saída caso não exista.
	if not os.path.exists(local_csv_path):
		f = open(local_csv_path, 'w')
		f.write(file_header + '\n')
		f.close()
	
	# Define os parâmetros em uma versão reduzida para rodar mais rápido.
	Ns = [10**4, 10**5, 10**6]
	n_processes = [1, 2, 3, 4]
	n_repetitions = 5

for i in range(n_repetitions):
	for procs in n_processes:
		for N in Ns:
			if 'PBS_O_WORKDIR' in os.environ.keys():
				# No servidor com PBS.
				os.system('/usr/lib64/openmpi/bin/mpirun -np {} {} xydata/xydata_N{}'.format(procs, filename, N))
			else:
				# Rodando localmente: usa o mpirun pelo path e grava saída ao final do arquivo.
				os.system('mpirun -np {} {} xydata/xydata_N{} >> {}'.format(procs, filename, N, local_csv_path))


