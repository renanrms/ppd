/* ------------------------------------------------------------------------
 * FILE: least-squares-pt2pt.c
 *  
 * PROBLEM DESCRIPTION:
 *  The method of least squares is a standard technique used to find
 *  the equation of a straight line from a set of data. Equation for a
 *  straight line is given by 
 *	 y = mx + b
 *  where m is the slope of the line and b is the y-intercept.
 *
 *  Given a set of n points {(x1,y1), x2,y2),...,xn,yn)}, let
 *      SUMx = x1 + x2 + ... + xn
 *      SUMy = y1 + y2 + ... + yn
 *      SUMxy = x1*y1 + x2*y2 + ... + xn*yn
 *      SUMxx = x1*x1 + x2*x2 + ... + xn*xn
 *
 *  The slope and y-intercept for the least-squares line can then be 
 *  calculated using the following equations:
 *        slope (m) = ( SUMx*SUMy - n*SUMxy ) / ( SUMx*SUMx - n*SUMxx ) 
 *  y-intercept (b) = ( SUMy - slope*SUMx ) / n
 *
 * PROGRAM DESCRIPTION:
 *  o This program computes a linear model for a set of given data.
 *  o Data is read from a file; the first line is the number of data 
 *    points (n), followed by the coordinates of x and y.
 *  o Data points are divided amongst processes such that each process
 *    has naverage = n/numprocs points; remaining data points are
 *    added to the last process. 
 *  o Each process calculates the partial sums (mySUMx,mySUMy,mySUMxy,
 *    mySUMxx) independently, using its data subset. In the final step,
 *    the global sums (SUMx,SUMy,SUMxy,SUMxx) are calculated to find
 *    the least-squares line. 
 *  o For the purpose of this exercise, communication is done strictly 
 *    by using the MPI point-to-point operations, MPI_SEND and MPI_RECV. 
 *  
 * USAGE: Tested to run using 1,2,...,10 processes.
 * 
 * AUTHOR: Dora Abdullah (MPI version, 11/96)
 * LAST REVISED: RYL converted to C (12/11)
 * ---------------------------------------------------------------------- */
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <stdio.h>
#include "mpi.h"

int main(int argc, char **argv)
{

	double *x, *y;
	double mySUMx, mySUMy, mySUMxy, mySUMxx, SUMx = 0, SUMy = 0, SUMxy = 0, SUMxx = 0, SUMres, res, slope, y_intercept, y_estimate;
	double tempo_inicial = 0.0, tempo_final;
	int i, j, n, myid, numprocs, naverage, nremain, mypoints, ishift = 0, tam_buffer, tam1, tam2, tam3;
	void *buffer;
	char *fileName;

	MPI_Status istatus;
	FILE *infile;

	if (argc != 2)
	{
		printf("Esperava-se 1 argumento mas %d foram dados.\n", argc - 1);
		return 1;
	}

	fileName = argv[1];

	infile = fopen(fileName, "r");
	if (infile == NULL)
	{
		printf("error opening file '%s'.\n", fileName);
		return 1;
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

	/* ----------------------------------------------------------
   * Step 1: Process 0 reads data and sends the value of n
   * ---------------------------------------------------------- */
	if (myid == 0)
	{
		if (fscanf(infile, "%d", &n) != 1)
			return 1;
	}

	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

	naverage = n / numprocs;
	nremain = n % numprocs;

	/* ----------------------------------------------------------
	 * Step 2: Process 0 sends subsets of x and y 
	 * ---------------------------------------------------------- */
	if (myid == 0)
	{
		x = (double *)malloc(n * sizeof(double));
		y = (double *)malloc(n * sizeof(double));

		for (i = 0; i < n; i++)
			if (fscanf(infile, "%lf %lf", &x[i], &y[i]) != 2)
				return 1;

		tempo_inicial = MPI_Wtime();

		/* Aloca e anexa o buffer */
		MPI_Pack_size(1, MPI_INT, MPI_COMM_WORLD, &tam1);
		MPI_Pack_size(naverage, MPI_DOUBLE, MPI_COMM_WORLD, &tam2);
		MPI_Pack_size(naverage + nremain, MPI_DOUBLE, MPI_COMM_WORLD, &tam3);
		tam_buffer = (numprocs - 2) * (2 * tam1 + 2 * tam2 + 4 * MPI_BSEND_OVERHEAD);
		tam_buffer += 1 * (2 * tam1 + 2 * tam3 + 4 * MPI_BSEND_OVERHEAD);
		buffer = (void *)malloc(tam_buffer);
		MPI_Buffer_attach(buffer, tam_buffer);

		for (i = 1; i < numprocs; i++)
		{
			/* Envia os valores que cada processo irá processar */
			ishift = i * naverage;
			mypoints = (i < numprocs - 1) ? naverage : naverage + nremain;
			MPI_Bsend(&mypoints, 1, MPI_INT, i, 2, MPI_COMM_WORLD);
			MPI_Bsend(&x[ishift], mypoints, MPI_DOUBLE, i, 3, MPI_COMM_WORLD);
			MPI_Bsend(&y[ishift], mypoints, MPI_DOUBLE, i, 4, MPI_COMM_WORLD);
		}

		ishift = 0;
		mypoints = naverage;
	}
	else
	{
		/* ---------------the other processes receive---------------- */
		ishift = 0;

		tempo_inicial = MPI_Wtime();

		MPI_Recv(&mypoints, 1, MPI_INT, 0, 2, MPI_COMM_WORLD, &istatus);

		x = (double *)malloc(mypoints * sizeof(double));
		y = (double *)malloc(mypoints * sizeof(double));

		MPI_Recv(x, mypoints, MPI_DOUBLE, 0, 3, MPI_COMM_WORLD,
				 &istatus);
		MPI_Recv(y, mypoints, MPI_DOUBLE, 0, 4, MPI_COMM_WORLD,
				 &istatus);
		/* ---------------------------------------------------------- */
	}

	/* ----------------------------------------------------------
   * Step 3: Each process calculates its partial sum
   * ---------------------------------------------------------- */
	mySUMx = 0;
	mySUMy = 0;
	mySUMxy = 0;
	mySUMxx = 0;
	for (j = 0; j < mypoints; j++)
	{
		mySUMx = mySUMx + x[ishift + j];
		mySUMy = mySUMy + y[ishift + j];
		mySUMxy = mySUMxy + x[ishift + j] * y[ishift + j];
		mySUMxx = mySUMxx + x[ishift + j] * x[ishift + j];
	}

	/* ----------------------------------------------------------
	 * Step 4: Process 0 receives reduction sums 
	 * ---------------------------------------------------------- */
	MPI_Reduce(&mySUMx, &SUMx, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mySUMy, &SUMy, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mySUMxy, &SUMxy, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&mySUMxx, &SUMxx, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	tempo_final = MPI_Wtime();

	/* ----------------------------------------------------------
	 * Step 5: Process 0 does the final steps
	 * ---------------------------------------------------------- */
	if (myid == 0)
	{
		slope = (SUMx * SUMy - n * SUMxy) / (SUMx * SUMx - n * SUMxx);
		y_intercept = (SUMy - slope * SUMx) / n;

		SUMres = 0;
		for (i = 0; i < n; i++)
		{
			y_estimate = slope * x[i] + y_intercept;
			res = y[i] - y_estimate;
			SUMres = SUMres + res * res;
		}

		/* Registra medidas de desempenho */
		printf("%i,%i,%f\n", n, numprocs, tempo_final - tempo_inicial);

		MPI_Buffer_detach(&buffer, &tam_buffer);
		free(buffer);
	}

	/* ----------------------------------------------------------	*/
	MPI_Finalize();
	return 0;
}
