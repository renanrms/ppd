#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>

int primo(long unsigned n)
{
	long unsigned i;
	for (i = 3; i < (long unsigned)(sqrt(n) + 1); i += 2)
	{
		if (n % i == 0)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	double t_inicial, t_final;
	long unsigned cont = 0, total = 0;
	long unsigned i, n, inicio, tamanho;
	int meu_ranque, num_procs, dest, raiz = 0, tag = 1, stop = 0;
	MPI_Status estado;

	/* Verifica o número de argumentos passados */
	if (argc < 2)
	{
		printf("Entre com o valor do maior inteiro como parâmetro para o programa.\n");
		return 0;
	}
	else
	{
		n = strtol(argv[1], (char **)NULL, 10);
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	/* Se houver menos que dois processos aborta */
	if (num_procs < 2)
	{
		printf("Este programa deve ser executado com no mínimo dois processos.\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
		return (1);
	}

	tamanho = (n / num_procs) / 100; /* Garante boa distribuição de carga */
	
	/* Garante que tamanho não seja nulo e tenha um valor mínimo */
	if (tamanho < 100)
	{
		tamanho = 100;
	}

	/* Registra o tempo inicial de execução do programa */
	t_inicial = MPI_Wtime();

	/* Envia pedaços com tamanho números para cada processo */
	if (meu_ranque == 0)
	{
		for (dest = 1, inicio = 3; dest < num_procs; dest++, inicio += tamanho)
		{
			if (inicio > n)
			{
				tag = 99;
				stop++;
			}

			MPI_Send(&inicio, 1, MPI_UNSIGNED_LONG, dest, tag, MPI_COMM_WORLD);
		}

		/* Fica recebendo as contagens parciais de cada processo */
		while (stop < (num_procs - 1))
		{
			MPI_Recv(&cont, 1, MPI_UNSIGNED_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);
			total += cont;
			dest = estado.MPI_SOURCE;

			if (inicio > n)
			{
				tag = 99;
				stop++;
			}

			/* Envia um nvo pedaço com tamanho números para o mesmo processo*/
			MPI_Send(&inicio, 1, MPI_UNSIGNED_LONG, dest, tag, MPI_COMM_WORLD);

			inicio += tamanho;
		}
	}
	else
	{
		/* Cada processo escravo recebe o início do espaço de busca */
		while (estado.MPI_TAG != 99)
		{
			MPI_Recv(&inicio, 1, MPI_UNSIGNED_LONG, raiz, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);

			if (estado.MPI_TAG != 99)
			{
				for (i = inicio, cont = 0; i < (inicio + tamanho) && i < n; i += 2)
					if (primo(i) == 1)
						cont++;

				/* Envia a contagem parcial para o processo mestre */
				MPI_Send(&cont, 1, MPI_UNSIGNED_LONG, raiz, tag, MPI_COMM_WORLD);
			}
		}
	}

	/* Registra o tempo final de execução */
	t_final = MPI_Wtime();

	if (meu_ranque == 0)
	{
		/* Acrescenta o dois, que também é primo */
		total += 1;

		/* Registra as medidas de desempenho */
		printf("%lu,%i,%lf\n", n, num_procs, t_final - t_inicial);
	}

	/* Finaliza o programa */
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	return (0);
}
