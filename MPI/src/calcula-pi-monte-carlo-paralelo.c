#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"
#define SEED time(NULL)

int main(int argc, char *argv[]) { 
    double pi,x,y,z;
    int count=0,total_count=0; 
    double t_inicial=0, t_final;
    long i, n;
    int meu_ranque, num_procs;

    if (argc !=2) return 1;
    n = atoll(argv[1]);


    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

    if (meu_ranque == 0) 
        t_inicial = MPI_Wtime();

    for (i = meu_ranque; i < n; i+=num_procs){

        x = (double)rand() / RAND_MAX;

        y = (double)rand() / RAND_MAX;

        z = x * x + y * y;

        if( z <= 1 ) count++;
    }

    MPI_Reduce(&count, &total_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (meu_ranque == 0) {
        pi = (double)(total_count) /n*4;
		t_final = MPI_Wtime();
		printf("%li,%i,%f\n", n, num_procs,t_final - t_inicial);
	}
    MPI_Finalize();
    return 0;
}
