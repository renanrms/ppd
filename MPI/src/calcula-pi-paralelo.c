#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]) {
    double pi = 0.0f,total; 
    double t_inicial=0, t_final;
    long i,n;
    int meu_ranque, num_procs;

    if (argc !=2) return 1;
    n = atoll(argv[1]);

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);


    if (meu_ranque == 0) {
        t_inicial = MPI_Wtime();
    }

    for (i = meu_ranque; i < n; i+=num_procs) {
        double t = (double) ((i+0.5)/n);
        pi += 4.0/(1.0+t*t);
    }

    MPI_Reduce(&pi, &total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (meu_ranque == 0) {
		t_final = MPI_Wtime();
		printf("%li,%i,%f\n", n, num_procs, t_final - t_inicial);
	}
    MPI_Finalize();
    return 0;
}
