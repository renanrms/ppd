#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <math.h>

int primo(long unsigned n)
{
	long unsigned i;

	for (i = 3; i < (long unsigned)(sqrt(n) + 1); i += 2)
	{
		if (n % i == 0)
			return 0;
	}
	return 1;
}

int main(int argc, char *argv[])
{
	double t_inicial, t_final;
	long unsigned cont = 0, total = 0;
	long unsigned i, n, inicio, salto;
	int meu_ranque, num_procs;

	if (argc < 2)
	{
		printf("Valor inválido! Entre com um valor do maior inteiro\n");
		return 0;
	}
	else
	{
		n = strtoul(argv[1], (char **)NULL, 10);
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	t_inicial = MPI_Wtime();

	inicio = 3 + meu_ranque * 2;
	salto = num_procs * 2;

	for (i = inicio; i <= n; i += salto)
	{
		if (primo(i) == 1)
			cont++;
	}

	if (num_procs > 1)
	{
		MPI_Reduce(&cont, &total, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
	}
	else
	{
		total = cont;
	}

	t_final = MPI_Wtime();

	if (meu_ranque == 0)
	{
		/* Acrescenta o dois, que também é primo */
		total += 1;

		/* Registra as medidas de desempenho */
		printf("%lu,%i,%lf\n", n, num_procs, t_final - t_inicial);
	}

	MPI_Finalize();

	return 0;
}