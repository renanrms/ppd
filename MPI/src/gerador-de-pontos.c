#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

#define ERROR_PRECISION 1000000
#define MAX_FILE_NAME_LENGTH 256

double f(double x)
{
	return x * x;
}

inline double error(double absDev)
{
	return ((rand() % (int)(2 * absDev * ERROR_PRECISION + 1)) - absDev * ERROR_PRECISION) / (double)ERROR_PRECISION;
}

int main(int argc, char *argv[])
{
	double x, y, xi, xf;
	long long N, i;
	char fileName[MAX_FILE_NAME_LENGTH];
	FILE *outputFile;

	srand((unsigned)time(NULL));

	if (argc != 4)
	{
		printf("Esperava-se 3 argumentos, mas %d foram passados.\n", argc - 1);
		printf("\nUso: %s xi xf N\n\n", argv[0]);
		printf("\txi\té o valor de partida da coordenada x.\n");
		printf("\txf\té o valor de final da coordenada x.\n");
		printf("\tN\té a quantidade de pontos.\n");
		return 1;
	}

	xi = atof(argv[1]);
	xf = atof(argv[2]);
	N = atoll(argv[3]);

	mkdir("xydata", (mode_t)0777);

	snprintf(fileName, MAX_FILE_NAME_LENGTH, "./xydata/xydata_N%lli", N);
	outputFile = fopen(fileName, "w");
	if (outputFile == NULL)
	{
		printf("Não foi possível criar o arquivo.\n");
		return 1;
	}
	fprintf(outputFile, "%llu", N);

	for (i = 0; i < N; i++)
	{
		x = (((N - 1) - i) * xi + i * xf) / (double)(N - 1);
		y = f(x) + error(0.5);
		fprintf(outputFile, "\n%lf %lf", x, y);
	}

	fclose(outputFile);

	return 0;
}