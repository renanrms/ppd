#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]) { 
    double pi,total=0;
    double t_inicial=0, t_final;
    long i,n;
    int meu_ranque, num_procs, tam;
    int dest=1, inicio=0, stop=0, tag=1;
    MPI_Status estado;

    if (argc !=2) return 1;
    n = atoll(argv[1]);

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);


    tam = n / (num_procs*10); // divide em blocos menores que num_procs

    if (meu_ranque == 0) {
        t_inicial = MPI_Wtime();
        for (dest=1, inicio=0; dest < num_procs && inicio < n; dest++, inicio += tam){
            MPI_Send(&inicio, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
        }
        
        while (stop<(num_procs-1)){
		    MPI_Recv(&pi, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);
            total += pi;
            dest = estado.MPI_SOURCE;
            if (inicio > n) {
                tag = 99;
                stop++;
            }
            MPI_Send(&inicio, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
            inicio += tam;
        }
    }
    else{
        while (estado.MPI_TAG != 99) {
            MPI_Recv(&inicio, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &estado);

            if (estado.MPI_TAG != 99) {
                pi = 0;
                for (i = inicio; i < (inicio + tam) && i<n ; i++){
                    double t = (double) ((i+0.5)/n);
                    pi += 4.0/(1.0+t*t);
                }
                MPI_Send(&pi, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
            } 
        }
    }

    if (meu_ranque ==0){
		t_final = MPI_Wtime();
        // printf("valor de pi:%f\n",total/n);
		printf("%li,%i,%f\n", n, num_procs, t_final - t_inicial);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
