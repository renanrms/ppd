#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <mpi.h>

void mergeArrays(int arr1[], int arr2[], int n1, int n2, int arr3[]) 
{
	int i = 0, j = 0, k = 0; 

	while (i<n1 && j<n2) 
	{ 
		if (arr1[i] < arr2[j]) 
			arr3[k++] = arr1[i++]; 
		else
			arr3[k++] = arr2[j++]; 
	} 

	while (i < n1) 
		arr3[k++] = arr1[i++]; 

	while (j < n2) 
		arr3[k++] = arr2[j++]; 
}

void mergeKArrays(int *arr[], int i, int j, int m, int output[])
{
	
	if(i==j)
	{
		for(int p=0; p < m; p++)
		output[p]=arr[i][p];
		return;
	}
	
	if(j-i==1)
	{
		mergeArrays(arr[i],arr[j],m,m,output);
		return;
	}
	
	int *out1 = (int*)malloc((m*(((i+j)/2)-i+1))*sizeof(int));
    int *out2 = (int*)malloc((m*(j-((i+j)/2)))*sizeof(int));
	
	mergeKArrays(arr,i,(i+j)/2,m,out1);
	mergeKArrays(arr,(i+j)/2+1,j,m,out2);
	
	mergeArrays(out1,out2,m*(((i+j)/2)-i+1),m*(j-((i+j)/2)),output);
	
}

int compare (const void *a, const void *b)
{
    return (*(int*)a - *(int*)b);
}

int main(int argc, char *argv[]) {
int i, j, k, meu_ranque, num_procs, raiz = 0, n, tam;
int *vet_envia, *vet_recebe, *vet_final;
double tempo_inicial, tempo_final;

    if (argc < 2) {
        return 0;
    } else {
        n = atoi(argv[1]);
    }

	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    tam = n/num_procs;
    vet_envia = (int*)malloc(num_procs*tam*sizeof(int));
    vet_recebe = (int*)malloc(tam*sizeof(int));
    vet_final = (int*)malloc(num_procs*tam*sizeof(int));
    
    srand(time(NULL));
    if (meu_ranque == raiz) {
        for (i = 0; i < (tam*num_procs); i++) {
            vet_envia[i] = ((float)rand()/RAND_MAX)*100;
        }
        tempo_inicial = MPI_Wtime();
    }
    
    MPI_Scatter(vet_envia, tam, MPI_INT, vet_recebe, tam, MPI_INT, raiz, MPI_COMM_WORLD);
    qsort(vet_recebe, tam, sizeof(int), compare);
    MPI_Gather(vet_recebe, tam, MPI_INT, vet_final, tam, MPI_INT, raiz, MPI_COMM_WORLD);

    if(meu_ranque == raiz) {
        int *merged[num_procs];
        int *out = (int*)malloc(num_procs*tam*sizeof(int));

        for(i=0; i<num_procs; i++) {
            merged[i] = malloc(tam*sizeof(int));
        }

        for(i=0, k=0; i<num_procs; i++) {
            for(j=0; j<tam; j++, k++) {
                merged[i][j] = vet_final[k];
            }
        }
        mergeKArrays(merged, 0, num_procs-1, tam, out);
        free(out);
    }

    free(vet_envia);
    free(vet_recebe);
    free(vet_final);

    if(meu_ranque == raiz) {
        tempo_final = MPI_Wtime();
        printf("Tempo total: %.3f\n", tempo_final-tempo_inicial);
    }
	MPI_Finalize();
    
	return(0);
}