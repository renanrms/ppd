#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mpi.h"

typedef struct complextype
{
	float real, imag;
} Compl;

int main(int argc, char **argv)
{
	long x_resn, y_resn;
	double t_inicial = 0, t_final;
	int meu_ranque, num_procs;

	if (argc != 2)
		return 1;
	x_resn = atoll(argv[1]);
	y_resn = x_resn;

	MPI_Request request[x_resn];
	int linha_envia[x_resn], linha_recebe[x_resn];

	Window win;					/* initialization for a window */
	unsigned int width, height, /* window size */
		x, y,					/* window position */
		border_width,			/*border width in pixels */
		screen;					/* which screen */

	char *window_name = "Mandelbrot Set", *display_name = NULL;
	GC gc;
	unsigned long valuemask = 0;
	XGCValues values;
	Display *display;
	XSizeHints size_hints;
	XSetWindowAttributes attr[1];

	/* Mandlebrot variables */
	int i, j, k;
	Compl z, c;
	float lengthsq, temp;

	/* connect to Xserver */
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &meu_ranque);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	if (meu_ranque == 0)
	{
		display = XOpenDisplay(display_name);

		if (display != NULL)
		{

			/* get screen size */

			screen = DefaultScreen(display);

			/* set window size */

			width = x_resn;
			height = y_resn;

			/* set window position */

			x = 0;
			y = 0;

			/* create opaque window */

			border_width = 4;
			win = XCreateSimpleWindow(display, RootWindow(display, screen),
									  x, y, width, height, border_width,
									  BlackPixel(display, screen), WhitePixel(display, screen));

			size_hints.flags = USPosition | USSize;
			size_hints.x = x;
			size_hints.y = y;
			size_hints.width = width;
			size_hints.height = height;
			size_hints.min_width = 300;
			size_hints.min_height = 300;

			XSetNormalHints(display, win, &size_hints);
			XStoreName(display, win, window_name);

			/* create graphics context */

			gc = XCreateGC(display, win, valuemask, &values);

			XSetBackground(display, gc, WhitePixel(display, screen));
			XSetForeground(display, gc, BlackPixel(display, screen));
			XSetLineAttributes(display, gc, 1, LineSolid, CapRound, JoinRound);

			attr[0].backing_store = Always;
			attr[0].backing_planes = 1;
			attr[0].backing_pixel = BlackPixel(display, screen);

			XChangeWindowAttributes(display, win, CWBackingStore | CWBackingPlanes | CWBackingPixel, attr);

			XMapWindow(display, win);
			XSync(display, 0);
		}
	}

	/* Calculate and draw points */

	t_inicial = MPI_Wtime();
	/*os processos secundarios calculam */
	if (meu_ranque != 0)
	{
		for (i = 0; i < x_resn; i += (num_procs - 1))
		{
			for (j = 0; j < y_resn; j++)
			{

				z.real = z.imag = 0.0;
				c.real = ((float)j - 400.0) / 200.0; /* scale factors for 800 x 800 window */
				c.imag = ((float)i - 400.0) / 200.0;
				k = 0;

				do
				{
					/* iterate for pixel color */
					temp = z.real * z.real - z.imag * z.imag + c.real;
					z.imag = 2.0 * z.real * z.imag + c.imag;
					z.real = temp;
					lengthsq = z.real * z.real + z.imag * z.imag;
					k++;

				} while (lengthsq < 4.0 && k < 100);
				linha_envia[j] = (k == 100) ? 1 : 0;
			}
			MPI_Isend(&linha_envia, x_resn, MPI_INT, 0, i, MPI_COMM_WORLD, &request[i]);
		}
	}
	else
	{
		for (i = 0; i < x_resn; i++)
		{
			MPI_Irecv(linha_recebe, x_resn, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,  &request[i]);
			MPI_Wait(&request[i],MPI_STATUS_IGNORE);
			for (j = 0; j < y_resn; j++)
			{
				if (linha_recebe[j] == 1 && display != NULL)
				{
					XDrawPoint(display, win, gc, j, i);
				}
			}
		}

		t_final = MPI_Wtime();
		if (display != NULL)
		{
			XFlush(display);
			// sleep(10);
		}
		printf("%li,%i,%f\n", x_resn, num_procs, t_final - t_inicial);
	}

	/* Program Finished */
	MPI_Finalize();
	return 0;
}
