#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define SEED time(NULL)

int main(int argc, char *argv[]) { 
    double pi,x,y,z;
    int count=0; 
    double t_inicial=0, t_final;
    long i, n;

    if (argc !=2) return 1;
    n = atoll(argv[1]);

    t_inicial = omp_get_wtime();

    for(i = 0; i < n; i++){
        x = (double)rand() / RAND_MAX;
        y = (double)rand() / RAND_MAX;
        z = x * x + y * y;
        if( z <= 1 ) count++;
    }
    
    pi = (double)(count) /n*4;
    t_final = omp_get_wtime();
    printf("%f",pi);
    printf("%li,%f\n", n, t_final - t_inicial);
    return 0;
}
