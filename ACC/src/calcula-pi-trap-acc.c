#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char *argv[]) {
    double pi = 0.0f,total; 
    double t, t_inicial=0, t_final;
    long i,n;

    if (argc !=2) return 1;
    n = atoll(argv[1]);

    t_inicial = omp_get_wtime();

    #pragma acc parallel loop reduction(+:pi)
    for (i = 0; i < n; i++) {
        t = (i+0.5)/n;
        pi += 4.0/(1.0+t*t);
    }

    t_final = omp_get_wtime();
    printf("%f\n",pi/n);
    printf("%li,%f\n", n, t_final - t_inicial);
    return 0;
}
