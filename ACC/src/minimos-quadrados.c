/* ------------------------------------------------------------------------
 * FILE: least-squares-pt2pt.c
 *  
 * PROBLEM DESCRIPTION:
 *  The method of least squares is a standard technique used to find
 *  the equation of a straight line from a set of data. Equation for a
 *  straight line is given by 
 *	 y = mx + b
 *  where m is the slope of the line and b is the y-intercept.
 *
 *  Given a set of n points {(x1,y1), x2,y2),...,xn,yn)}, let
 *      SUMx = x1 + x2 + ... + xn
 *      SUMy = y1 + y2 + ... + yn
 *      SUMxy = x1*y1 + x2*y2 + ... + xn*yn
 *      SUMxx = x1*x1 + x2*x2 + ... + xn*xn
 *
 *  The slope and y-intercept for the least-squares line can then be 
 *  calculated using the following equations:
 *        slope (m) = ( SUMx*SUMy - n*SUMxy ) / ( SUMx*SUMx - n*SUMxx ) 
 *  y-intercept (b) = ( SUMy - slope*SUMx ) / n
 *
 * PROGRAM DESCRIPTION:
 *  o This program computes a linear model for a set of given data.
 *  o Data is read from a file; the first line is the number of data 
 *    points (n), followed by the coordinates of x and y.
 *  o Data points are divided amongst processes such that each process
 *    has naverage = n/numprocs points; remaining data points are
 *    added to the last process. 
 *  o Each process calculates the partial sums (mySUMx,mySUMy,mySUMxy,
 *    mySUMxx) independently, using its data subset. In the final step,
 *    the global sums (SUMx,SUMy,SUMxy,SUMxx) are calculated to find
 *    the least-squares line. 
 *  o For the purpose of this exercise, communication is done strictly 
 *    by using the MPI point-to-point operations, MPI_SEND and MPI_RECV. 
 *  
 * ---------------------------------------------------------------------- */

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <omp.h>

int main(int argc, char **argv)
{

	double *x, *y;
	double SUMx, SUMy, SUMxy, SUMxx, SUMres, res, slope, y_intercept, y_estimate;
	double tempo_inicial, tempo_final;
	int i, n, num_gangs, vector_length;
	char *fileName;

	FILE *infile;

    if (argc != 4)
    {
        printf("Esperava-se 3 argumentos, mas %d foram passados.\n", argc - 1);
        printf("\nUso: %s num_gangs vector_length fileName\n\n", argv[0]);
        printf("\tnum_gangs\té o número de gangues.\n");
        printf("\tvector_length\té o comprimento de vetor na execução paralela.\n");
        printf("\tfileName\té o nome do arquivo com o conjunto de pontos.\n");
        return 1;
    }
    num_gangs = atoi(argv[1]);
    vector_length = atoi(argv[2]);
	fileName = argv[3];


	infile = fopen(fileName, "r");
	if (infile == NULL)
	{
		printf("error opening file '%s'.\n", fileName);
		return 1;
	}

	/* ----------------------------------------------------------
   * Step 1: Lê o arquivo
   * ---------------------------------------------------------- */
	if (fscanf(infile, "%d", &n) != 1)
	{
		return 1;
	}
	
	x = (double *)malloc(n * sizeof(double));
	y = (double *)malloc(n * sizeof(double));
	
	for (i = 0; i < n; i++)
		if (fscanf(infile, "%lf %lf", &x[i], &y[i]) != 2)
		{
			return 1;
		}

	/* ----------------------------------------------------------
   * Step 2: Calcula
   * ---------------------------------------------------------- */

	tempo_inicial = omp_get_wtime();

	SUMx = 0;
	SUMy = 0;
	SUMxy = 0;
	SUMxx = 0;

	//#pragma acc parallel copyin(x[:n],y[:n]) copy(SUMy,SUMxx,SUMxy,SUMx) num_gangs(num_gangs) vector_length(vector_length)
	#pragma acc parallel copyin(x[:n],y[:n]) copy(SUMy,SUMxx,SUMxy,SUMx)
	{
		#pragma acc loop gang reduction(+:SUMx,SUMxx,SUMy,SUMxy)
		for (i = 0; i < n; i++)
		{
			SUMx = SUMx + x[i];
			SUMy = SUMy + y[i];
			SUMxy = SUMxy + x[i] * y[i];
			SUMxx = SUMxx + x[i] * x[i];
		}
	}

	slope = (SUMx * SUMy - n * SUMxy) / (SUMx * SUMx - n * SUMxx);
	y_intercept = (SUMy - slope * SUMx) / n;

	SUMres = 0;

	tempo_final = omp_get_wtime();

	/* ----------------------------------------------------------
   * Step 3: Passos finais
   * ---------------------------------------------------------- */
	
	for (i = 0; i < n; i++)
	{
		y_estimate = slope * x[i] + y_intercept;
		res = y[i] - y_estimate;
		SUMres = SUMres + res * res;
	}

	/* Registra medidas de desempenho */
    printf("%li,%i,%i,%lf\n", n, num_gangs, vector_length, tempo_final - tempo_inicial);

	return 0;
}
