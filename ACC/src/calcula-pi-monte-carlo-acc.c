#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define SEED time(NULL)

int main(int argc, char *argv[])
{
    double pi, x, y, z, *rand_arr;
    int count = 0;
    double t_inicial = 0, t_final;
    long i, n;

    if (argc != 2)
        return 1;
    n = atoll(argv[1]);

    rand_arr = (double *)malloc(n * 2 * sizeof(double));

    for (i = 0; i < n * 2; i++)
    {
        rand_arr[i] = (double)rand() / RAND_MAX;
    }
    t_inicial = omp_get_wtime();

#pragma acc data copyin(rand_arr [0:n * 2])
#pragma acc parallel loop gang reduction(+ : count)
    for (i = 0; i < n; i++)
    {
        x = rand_arr[i];
        y = rand_arr[i + n];
        z = x * x + y * y;
        if (z <= 1)
            count++;
    }
    pi = (double)(count) / n * 4;
    t_final = omp_get_wtime();
    printf("%f", pi);
    printf("%li,%f\n", n, t_final - t_inicial);
    return 0;
}
