#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void initialize(int *vector, long length);

int main(int argc, char *argv[])
{
    long N, i, j, count;
    int *original, *ordered, num_gangs, vector_length;
    double t_start, t_end;

    if (argc != 4)
    {
        printf("Esperava-se 3 argumentos, mas %d foram passados.\n", argc - 1);
        printf("\nUso: %s num_gangs vector_length N\n\n", argv[0]);
        printf("num_gangs\té o número de gangues.\n");
        printf("vector_length\té o comprimento de vetor na execução paralela.\n");
        printf("\tN\té o tamanho do vetor a ser ordenado.\n");
        return 1;
    }
    num_gangs = atoi(argv[1]);
    vector_length = atoi(argv[2]);
    N = atol(argv[3]);

    original = (int *)malloc(N * sizeof(int));
    initialize(original, N);

    t_start = omp_get_wtime();

    ordered = (int *)malloc(N * sizeof(int));
    
    #pragma acc parallel copyin(original[:N]) copyout(ordered[:N])
    //#pragma acc parallel copyin(original[:N]) copyout(ordered[:N]) num_gangs(num_gangs) vector_length(vector_length)
    {
        #pragma acc loop gang
        for (i = 0; i < N; i++)
        {
            count = 0;

            /* Testa o original[i] em relação a todos os outros valores */
            #pragma acc loop vector reduction(+ : count)
            for (j = 0; j < N; j++)
            {
                if(original[j] < original[i])
                {
                    count++;
                }
            }

            /* Insere na posição certa do vetor */
            ordered[count] = original[i];
        }
    }

    t_end = omp_get_wtime();

    printf("%li,%i,%i,%lf\n", N, num_gangs, vector_length, t_end - t_start);

    /* Mostra os dois vetores para conferência */
    /*
    printf("Original:\n\n");
    for (i = 0; i < N; i++)
        printf("%i ", original[i]);
    printf("\n\n");
    printf("Ordenado:\n\n");
    for (i = 0; i < N; i++)
        printf("%i ", ordered[i]);
    printf("\n");
    */

   free(original);
   free(ordered);

    return 0;
}

void initialize(int *vector, long length)
{
    for (long i = 0; i < length; i++)
        vector[i] = rand();
}
