#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

filename = 'mandelbrot-acc'
#filename = 'mandelbrot-seq'

file_header = 'n,wtime'
local_csv_path = '../eval/{}.csv'.format(filename)

os.chdir(os.path.dirname(os.path.abspath(__file__)))
os.chdir('../bin')

with open(local_csv_path, 'w') as f:
	f.write(file_header + '\n')

Ns = [10**3, 10**4]
n_repetitions =5 

for i in range(n_repetitions):
	for N in Ns:
		args = '{} {}'.format(filename, N)
		os.system('./{} >> {}'.format(args, local_csv_path))

