#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

filename = 'minimos-quadrados'

file_header = 'n,num_gangs,vector_length,wtime'
local_csv_path = '../eval/{}.csv'.format(filename)

# Muda para o diretório do programa (para caso seja chamado de outro local).
os.chdir(os.path.dirname(os.path.abspath(__file__)))
os.chdir('../bin')

# Escreve o cabeçalho no arquivo de saída caso não exista.
if not os.path.exists(local_csv_path):
	f = open(local_csv_path, 'w')
	f.write(file_header + '\n')
	f.close()

# Define os parâmetros
Ns = [10**4, 10**5, 10**6]
n_gangs = [1, 2, 4, 8, 16, 32, 64, 128, 192]
#n_gangs = [1]
#vector_lengths = [1, 2, 4, 8, 16, 32, 64, 128]
vector_lengths = [1]
n_repetitions = 2

for i in range(n_repetitions):
	for gangs in n_gangs:
		for vector in vector_lengths:
			for N in Ns:

				# Define o argv (programa e argumentos) a ser chamado.
				args = '{} {} {} xydata/xydata_N{}'.format(filename, gangs, vector, N)

				# Rodando localmente: grava saída ao final do arquivo.
				os.system('./{} >> {}'.format(args, local_csv_path))
