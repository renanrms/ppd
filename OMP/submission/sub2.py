#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

filename = 'ranksort'

file_header = 'n,n_threads,wtime'
local_csv_path = '../eval/{}.csv'.format(filename)

if 'PBS_O_WORKDIR' in os.environ.keys(): # No servidor com PBS:

	# Vai para o diretório de onde se chamou o qsub depois entra na pasta bin.
	os.chdir(os.environ['PBS_O_WORKDIR'])
	os.chdir('../bin')

	# Printa o cabeçalho.
	print(file_header)

	# Define os parâmetros para rodar como descrito no trabalho.
	Ns = [10**5, 2*10**5, 4*10**5]
	n_threads = [1, 2, 4, 8, 12, 16]
	n_repetitions = 5

else: # Rodando localmente:

	# Muda para o diretório do programa (para caso seja chamado de outro local).
	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	os.chdir('../bin')
	
	# Escreve o cabeçalho no arquivo de saída caso não exista.
	if not os.path.exists(local_csv_path):
		f = open(local_csv_path, 'w')
		f.write(file_header + '\n')
		f.close()
	
	# Define os parâmetros em uma versão reduzida para rodar mais rápido.
	Ns = [4*10**4, 8*10**4, 16*10**4]
	n_threads = [1, 2, 4, 8]
	n_repetitions = 5

for i in range(n_repetitions):
	for threads in n_threads:
		for N in Ns:

			# Define o argv (programa e argumentos) a ser chamado.
			args = '{} {} {}'.format(filename, threads, N)

			if 'PBS_O_WORKDIR' in os.environ.keys():
				# No servidor com PBS.
				os.system('/work/grupo_c/ppd/OMP/bin/{}'.format(args))
			else:
				# Rodando localmente: grava saída ao final do arquivo.
				os.system('./{} >> {}'.format(args, local_csv_path))


