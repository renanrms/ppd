#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

#define COLUMNS 1022
#define ROWS 1022
#define MAX_TEMP_ERROR 0.01

double Anew[ROWS + 2][COLUMNS + 2];
double A[ROWS + 2][COLUMNS + 2];

void iniciar();

int main(int argc, char *argv[])
{
     int max_iterations = 1024, num_threads;
     int iteration = 1;
     double dt = 100, t_start, t_end;

     if (argc != 2)
          return 1;
     num_threads = atoi(argv[1]);

     omp_set_num_threads(num_threads);
     t_start = omp_get_wtime();
     iniciar();
     while (iteration <= max_iterations)
     {
          #pragma omp parallel for collapse(2) shared(Anew, A)
          for (int i = 1; i <= ROWS - 1; i++)
               for (int j = 1; j <= COLUMNS - 1; j++)
                    Anew[i][j] = 0.25 * (A[i + 1][j] + A[i - 1][j] + A[i][j + 1] + A[i][j - 1]);
          dt = 0.0;

          #pragma omp parallel for collapse(2) shared(Anew, A)
          for (int i = 1; i <= ROWS - 1; i++)
               for (int j = 1; j <= COLUMNS - 1; j++){
                    dt = fmax(fabs(Anew[i][j] - A[i][j]), dt);
                    A[i][j] = Anew[i][j];
               }
          iteration++;
          A[800][800] = 100.0;
     }
     t_end = omp_get_wtime();

     printf("%d,%f\n", num_threads, t_end - t_start);
     return (0);
}

void iniciar()
{
#pragma omp parallel for collapse(2) shared(A)
     for (int i = 0; i <= ROWS + 1; i++)
     {
          for (int j = 0; j <= COLUMNS + 1; j++)
          {
               A[i][j] = 20.0; //temp inicial
          }
     }
     A[800][800] = 100.0; //centro a 100graus
}
