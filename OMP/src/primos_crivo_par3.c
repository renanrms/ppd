#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

int main(int argc, char *argv[])
{ 
    long n, i, j, non_primes = 0, num_threads;
    int *a;
    double t_start, t_end;

    if (argc != 3) return 1;
    num_threads = atoi(argv[1]);
    n = atol(argv[2]);
    omp_set_num_threads(num_threads);

    a = calloc(n, sizeof(int));
    t_start = omp_get_wtime();

    #pragma omp parallel for schedule(static,10) private(i,j) firstprivate(n) shared(a) //reduction(&&:a[0:n]) 
    for (i = 3; i < n; i++) //realiza o crivo
        if (a[i] == 0)
            for (j = i * 2; j < n; j += i)
                a[j] = 1;    
    
    #pragma omp parallel for private(i) reduction(+ : non_primes)//conta os primos
    for (i = 3; i < n; i++)
        non_primes+=a[i];
    t_end = omp_get_wtime();

    // printf("%ld\n", n-non_primes-3);
    printf("%ld,%ld,%f\n", n, num_threads, t_end - t_start);
}