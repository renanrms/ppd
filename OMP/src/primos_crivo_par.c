#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

int main(int argc, char *argv[])
{ /* primos_crivo_seq.c  */
    long n, i, j, primes = 0, num_threads;
    int *a;
    double t_start, t_end;

    if (argc != 3) return 1;
    num_threads = atoi(argv[1]);
    n = atol(argv[2]);
    omp_set_num_threads(num_threads);

    a = (int *)malloc(n * sizeof(int));
    memset(a, 0, n * sizeof(int));

    t_start = omp_get_wtime();

    #pragma omp parallel for private(i,j) shared(a)
    for (i = 3; i < n; i++) //realiza o crivo
        for (j = i * 2; j < n; j += i)
            a[j] = 1;
    
    #pragma omp parallel for private(i) reduction(+ : primes)//conta os primos
    for (i = 3; i < n; i++)
        if (a[i]==0) primes++;
    t_end = omp_get_wtime();

    // printf("%ld\n", primes);
    printf("%ld,%ld,%f\n", n, num_threads, t_end - t_start);
}