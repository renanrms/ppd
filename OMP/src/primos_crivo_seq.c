#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
// #include <omp.h>

int main(int argc, char *argv[]) { /* primos_crivo_seq.c  */
    long n, i, j, primes=0;
    int* a;
    if (argc !=2) return 1;
    n = atoll(argv[1]);
    a = (int*)malloc(n * sizeof(int));
    primes = n-3; //primos começa com n-3 e dimini a cada não primo encontrado
    for(i=3; i<n; i++)
        for(j=i*2; j<n ; j+=i){ 
            if(a[j]!=1){ //cada não primo descoberto decrementa 1 de primos
                a[j]=1;
                primes--;
            } 
        }
    printf("número de primos entre 2 e %ld: %ld\n",n,primes);
}