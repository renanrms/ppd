#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

typedef struct complextype
{
	float real, imag;
} Compl;

int main(int argc, char **argv)
{
	int i, j, k;
	Compl z, c;
	float lengthsq, temp;

	long x_resn, y_resn;
	double t_start, t_end;
	int num_threads;
	int** a;

	if (argc != 3) return 1;
    num_threads = atoi(argv[1]);
    x_resn = atol(argv[2]);
    omp_set_num_threads(num_threads);
	y_resn = x_resn;

	Window win;					/* initialization for a window */
	unsigned int width, height, /* window size */
		x, y,					/* window position */
		border_width,			/*border width in pixels */
		screen;                 /* which screen */

	char *window_name = "Mandelbrot Set", *display_name = NULL;
	GC gc;
	unsigned long valuemask = 0;
	XGCValues values;
	Display *display;
	XSizeHints size_hints;
	XSetWindowAttributes attr[1];


	/* connect to Xserver */

	display = XOpenDisplay(display_name);

	if (display != NULL)
	{
		/* get screen size */

		screen = DefaultScreen(display);

		/* set window size */

		width = x_resn;
		height = y_resn;

		/* set window position */

		x = 0;
		y = 0;

		/* create opaque window */

		border_width = 4;
		win = XCreateSimpleWindow(display, RootWindow(display, screen),
								  x, y, width, height, border_width,
								  BlackPixel(display, screen), WhitePixel(display, screen));

		size_hints.flags = USPosition | USSize;
		size_hints.x = x;
		size_hints.y = y;
		size_hints.width = width;
		size_hints.height = height;
		size_hints.min_width = 300;
		size_hints.min_height = 300;

		XSetNormalHints(display, win, &size_hints);
		XStoreName(display, win, window_name);

		/* create graphics context */

		gc = XCreateGC(display, win, valuemask, &values);

		XSetBackground(display, gc, WhitePixel(display, screen));
		XSetForeground(display, gc, BlackPixel(display, screen));
		XSetLineAttributes(display, gc, 1, LineSolid, CapRound, JoinRound);

		attr[0].backing_store = Always;
		attr[0].backing_planes = 1;
		attr[0].backing_pixel = BlackPixel(display, screen);

		XChangeWindowAttributes(display, win, CWBackingStore | CWBackingPlanes | CWBackingPixel, attr);

		XMapWindow(display, win);
		XSync(display, 0);
	}


	a = malloc(x_resn * sizeof(*a));
	for (int i = 0; i < x_resn; i++) {
		a[i] = malloc(y_resn * sizeof(a[0]));
	}

	/* Calculate points */
	t_start = omp_get_wtime();

	#pragma omp parallel for private(i,j,k,z) collapse(2)
	for (i = 0; i < x_resn; i++)
		for (j = 0; j < y_resn; j++)
		{

			z.real = z.imag = 0.0;
			c.real = ((float)j - 400.0) / 200.0; /* scale factors for 800 x 800 window */
			c.imag = ((float)i - 400.0) / 200.0;
			k = 0;

			do
			{
				/* iterate for pixel color */
				temp = z.real * z.real - z.imag * z.imag + c.real;
				z.imag = 2.0 * z.real * z.imag + c.imag;
				z.real = temp;
				lengthsq = z.real * z.real + z.imag * z.imag;
				k++;

			} while (lengthsq < 4.0 && k < 100);
			if (k == 100) a[j][i]=1;
			else a[j][i]=0;
		}
	
	t_end = omp_get_wtime();//não conta tempo de desenhar 
	/* draw points */

	for (i = 0; i < x_resn; i++)
		for (j = 0; j < y_resn; j++)
			if(a[j][i] == 1)
				if (display != NULL)
					XDrawPoint(display, win, gc, j, i);
    printf("%ld,%d,%f\n", x_resn, num_threads, t_end - t_start);

	if (display != NULL)
	{
		XFlush(display);
		// sleep(30); //adicionar para ver imagem, remover quando for contar tempo
	}

	/* Program Finished */

	return 0;
}
