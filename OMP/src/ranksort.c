#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void initialize(int *vector, long length);

int main(int argc, char *argv[])
{
    long N, i, j, count;
    int *original, *ordered, num_threads;
    double t_start, t_end;

    if (argc != 3)
    {
        printf("Esperava-se 2 argumentos, mas %d foram passados.\n", argc - 1);
        printf("\nUso: %s threads N\n\n", argv[0]);
        printf("\tthreads\té o número de threads.\n");
        printf("\tN\té o tamanho do vetor a ser ordenado.\n");
        return 1;
    }
    num_threads = atoi(argv[1]);
    N = atol(argv[2]);

    omp_set_num_threads(num_threads);
    original = (int *)malloc(N * sizeof(int));
    initialize(original, N);

    t_start = omp_get_wtime();

    ordered = (int *)malloc(N * sizeof(int));
    
    #pragma omp parallel
    {
        #pragma omp for private(i, j, count)
        for (i = 0; i < N; i++)
        {
            count = 0;

            /* Testa o original[i] em relação a todos os outros valores */
            for (j = 0; j<N; j++)
            {
                if(original[j] < original[i])
                {
                    count++;
                }
            }

            /* Insere na posição certa do vetor */
            ordered[count] = original[i];
        }
    }

    t_end = omp_get_wtime();

    printf("%li,%i,%lf\n", N, num_threads, t_end - t_start);

    return 0;
}

void initialize(int *vector, long length)
{
    for (long i = 0; i < length; i++)
        vector[i] = rand();
}
